import cookieMiddleware, {
  getStateFromCookies
} from 'redux-cookies-middleware';

// configure information stored in cookies
const paths = {
  'auth.token': {
    name: 'token'
  }
};

export const getInitialState = defaultState =>
  getStateFromCookies(defaultState, paths);

export default cookieMiddleware(paths);
