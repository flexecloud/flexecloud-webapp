const HtmlWebPackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack = require('webpack');
const path = require('path');
require('dotenv').config();

const themeColor = '#2196f3';
const publicPath = '/';
const title = 'flexecloud';
const description = 'A cloud platform to operate electrical vehicle charging.';
const envVars = ['API_URL', 'TAG', 'POLL_INTERVAL'];

module.exports = env => {
  const config = {
    mode: env.mode,
    entry: './src/index',
    output: {
      filename: '[name].[hash].js',
      path: path.resolve(__dirname, 'dist'),
      publicPath
    },
    resolve: {
      extensions: ['.jsx', '.js', '.json']
    },
    module: {
      rules: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          loader: 'eslint-loader',
          enforce: 'pre'
        },
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          loader: 'babel-loader'
        },
        {
          test: /\.hbs$/,
          exclude: /node_modules/,
          loader: 'handlebars-loader'
        }
      ]
    },
    plugins: [
      new CleanWebpackPlugin(['dist'], {
        verbose: false
      }),
      new HtmlWebPackPlugin({
        xhtml: true,
        filename: './index.html',
        template: './public/index.hbs',
        minify: {
          collapseWhitespace: true,
          minifyCSS: true
        },
        title,
        description,
        themeColor
      }),
      new CopyWebpackPlugin([
        {
          from: './public',
          ignore: ['*.hbs', '*.svg']
        }
      ]),
      new webpack.EnvironmentPlugin(envVars)
    ],
    optimization: {
      runtimeChunk: 'single',
      splitChunks: {
        cacheGroups: {
          vendor: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendors',
            chunks: 'all'
          }
        }
      }
    }
  };

  if (env.mode === 'development') {
    config.plugins.push(new webpack.HotModuleReplacementPlugin());
    config.devServer = {
      historyApiFallback: true,
      contentBase: './dist',
      hot: true,
      overlay: true
    };
  }

  return config;
};
