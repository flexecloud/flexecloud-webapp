const adaptMixin = (mixin, oldProp, newProp) => {
  const adaptedStyles = {};
  Object.keys(mixin).forEach(prop => {
    if (prop === oldProp) {
      adaptedStyles[newProp] = mixin[oldProp];
    } else if (prop.charAt(0) === '@') {
      Object.keys(mixin[prop]).forEach(subprop => {
        if (subprop === oldProp) {
          adaptedStyles[prop] = {
            [newProp]: mixin[prop][oldProp]
          };
        }
      });
    }
  });
  return adaptedStyles;
};

const styles = theme => ({
  layout: {
    flexGrow: 1,
    height: '100vh',
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex'
  },
  main: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 2,
    overflow: 'auto',
    overflowY: 'scroll',
    ...adaptMixin(theme.mixins.toolbar, 'minHeight', 'marginTop')
  },
  iframe: {
    display: 'none'
  }
});

export default styles;
