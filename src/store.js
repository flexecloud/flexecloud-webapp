import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { auth, user, issue } from './services';
import axiosMiddleware from './middlewares/axios';
import cookieMiddleware, { getInitialState } from './middlewares/cookie';

// combine different reducers to store
const reducers = combineReducers({
  auth,
  user,
  issue
});

// fetch default state state defined in reducers
export const getDefaultState = createStore(reducers).getState;

// create initial state from default state and cookies
const initialState = getInitialState(getDefaultState());

// combine middlewares to enable usage in store
const middlewares = applyMiddleware(
  cookieMiddleware,
  axiosMiddleware,
  thunkMiddleware
);

export default createStore(reducers, initialState, middlewares);
