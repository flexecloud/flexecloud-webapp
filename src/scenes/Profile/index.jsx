import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Card,
  CardContent,
  CardHeader,
  Grid,
  LinearProgress,
  TextField,
  Typography
} from '@material-ui/core';
import { connect } from 'react-redux';
import { startCase } from 'lodash';
import { POLL_INTERVAL } from '../../constants';
import { FixedLayout, CheckboxSelection } from '../../components';
import { getPermissionName, getPermissions } from '../../util';
import { readMe } from '../../services/user/actions';

const permissions = getPermissions();

class Profile extends Component {
  constructor() {
    super();
    this.timer = null;
  }

  componentDidMount() {
    const { dispatchReadMe } = this.props;
    if (!this.timer) {
      this.timer = setInterval(dispatchReadMe, POLL_INTERVAL);
    }
  }

  componentWillUnmount() {
    if (this.timer) {
      clearInterval(this.timer);
      this.timer = null;
    }
  }

  render() {
    const { user } = this.props;
    return (
      <FixedLayout>
        <Card>
          {!user && <LinearProgress color="secondary" />}
          <CardHeader title="Profile" subheader="Your user information" />
          <CardContent>
            {user ? (
              <Grid spacing={24} container>
                <Grid xs={12} lg={6} item>
                  <TextField
                    value={user.displayName}
                    label="Display name"
                    variant="filled"
                    fullWidth
                    disabled
                  />
                </Grid>
                <Grid xs={12} lg={6} item>
                  <TextField
                    value={user.email}
                    label="Email"
                    variant="filled"
                    fullWidth
                    disabled
                  />
                </Grid>
                <Grid xs={12} lg={6} item>
                  <TextField
                    value={startCase(user.role)}
                    label="Role"
                    variant="filled"
                    fullWidth
                    disabled
                  />
                </Grid>
                <Grid xs={12} lg={6} item>
                  <TextField
                    value={
                      user.cardNumber
                        ? user.cardNumber.substring(user.cardNumber.length - 6)
                        : 'unknown'
                    }
                    label="Card number"
                    variant="filled"
                    fullWidth
                    disabled
                  />
                </Grid>
                <Grid xs={12} item>
                  <CheckboxSelection
                    disabled
                    helperText="Permissions"
                    transformLabels={getPermissionName}
                    options={permissions}
                    selected={user.permissions}
                  />
                </Grid>
              </Grid>
            ) : (
              <Typography>
                Your profile information is currently unavailable.
              </Typography>
            )}
          </CardContent>
        </Card>
      </FixedLayout>
    );
  }
}

Profile.propTypes = {
  user: PropTypes.shape({
    email: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired,
    role: PropTypes.string.isRequired,
    cardNumber: PropTypes.string.isRequired
  }),
  dispatchReadMe: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: null
};

const mapStateToProps = ({ user }) => ({ user: user.user });
const mapDispatchToProps = dispatch => ({
  dispatchReadMe: () => dispatch(readMe())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
