import React from 'react';
import PropTypes from 'prop-types';
import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  Grid,
  TextField,
  Typography
} from '@material-ui/core';
import { ExpandMore } from '@material-ui/icons';
import { startCase } from 'lodash';
import { connect } from 'react-redux';
import { CheckboxSelection } from '../../../../components';
import { getPermissions, getPermissionName } from '../../../../util';

const permissions = getPermissions();

const User = ({ id, user, expanded, onClick }) => (
  <ExpansionPanel expanded={expanded} onChange={onClick(id)}>
    <ExpansionPanelSummary expandIcon={<ExpandMore />}>
      <Grid container>
        <Grid xs={12} md={6} item>
          <Typography variant="subtitle2">{user.displayName}</Typography>
        </Grid>
        <Grid xs={12} md={6} item>
          <Typography variant="subtitle2">{user.email}</Typography>
        </Grid>
      </Grid>
    </ExpansionPanelSummary>
    <ExpansionPanelDetails>
      <Grid spacing={16} container>
        <Grid xs={12} md={6} item>
          <Grid spacing={16} direction="column" container>
            <Grid xs={12} item>
              <TextField
                value={startCase(user.role)}
                label="Role"
                variant="filled"
                fullWidth
                disabled
              />
            </Grid>
            <Grid xs={12} item>
              <TextField
                value={
                  user.cardNumber
                    ? user.cardNumber.substring(user.cardNumber.length - 6)
                    : 'unknown'
                }
                label="Card number"
                variant="filled"
                fullWidth
                disabled
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid xs={12} md={6} item>
          <CheckboxSelection
            disabled
            helperText="Permissions"
            transformLabels={getPermissionName}
            options={permissions}
            selected={user.permissions}
          />
        </Grid>
      </Grid>
    </ExpansionPanelDetails>
  </ExpansionPanel>
);

User.propTypes = {
  id: PropTypes.string.isRequired,
  user: PropTypes.shape({
    id: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired,
    role: PropTypes.string.isRequired,
    permissions: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
    cardNumber: PropTypes.string
  }).isRequired,
  expanded: PropTypes.bool,
  onClick: PropTypes.func
};

User.defaultProps = {
  expanded: false,
  onClick: () => null
};

const mapStateToProps = ({ user }, { id }) => ({
  user: user.users[id]
});

export default connect(mapStateToProps)(User);
