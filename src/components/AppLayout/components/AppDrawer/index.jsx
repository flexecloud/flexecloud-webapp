import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {
  Drawer,
  IconButton,
  ListItem,
  ListItemIcon,
  ListItemText,
  Divider,
  withStyles
} from '@material-ui/core';
import {
  ChevronLeft,
  Dashboard,
  Mail,
  Memory,
  Person,
  Group,
  FlashOn,
  DirectionsCar
} from '@material-ui/icons';
import { NavLink } from 'react-router-dom';
import styles from './styles';

const AppDrawer = ({ classes, open, toggle, user }) => {
  const drawerClasses = {
    paper: classnames(classes.drawerPaperDefault, {
      [classes.drawerPaperClose]: !open
    })
  };
  const managementRoles = ['staff', 'supervisor', 'admin'];
  return (
    <Drawer variant="permanent" classes={drawerClasses} open={open}>
      <div className={classes.drawerToolbar}>
        <IconButton onClick={toggle}>
          <ChevronLeft />
        </IconButton>
      </div>
      <Divider />
      <ListItem
        activeClassName={classes.activeListItem}
        component={NavLink}
        to="/dashboard"
        button
      >
        <ListItemIcon>
          <Dashboard />
        </ListItemIcon>
        <ListItemText primary="Dashboard" />
      </ListItem>
      <Divider />
      <ListItem
        activeClassName={classes.activeListItem}
        component={NavLink}
        to="/charging_stations"
        button
      >
        <ListItemIcon>
          <FlashOn />
        </ListItemIcon>
        <ListItemText primary="Charging stations" />
      </ListItem>
      <Divider />
      <ListItem
        activeClassName={classes.activeListItem}
        component={NavLink}
        to="/cars"
        button
      >
        <ListItemIcon>
          <DirectionsCar />
        </ListItemIcon>
        <ListItemText primary="Cars" />
      </ListItem>
      <Divider />
      {user && (
        <Fragment>
          <ListItem
            activeClassName={classes.activeListItem}
            component={NavLink}
            to="/approvals"
            button
          >
            <ListItemIcon>
              <Mail />
            </ListItemIcon>
            <ListItemText primary="Approvals" />
          </ListItem>
          <Divider />
        </Fragment>
      )}
      {user && user.role && managementRoles.includes(user.role) ? (
        <Fragment>
          <ListItem
            activeClassName={classes.activeListItem}
            component={NavLink}
            to="/users"
            button
          >
            <ListItemIcon>
              <Group />
            </ListItemIcon>
            <ListItemText primary="Users" />
          </ListItem>
          <Divider />
        </Fragment>
      ) : null}
      {user && user.role && managementRoles.includes(user.role) ? (
        <Fragment>
          <ListItem
            activeClassName={classes.activeListItem}
            component={NavLink}
            to="/nodes"
            button
          >
            <ListItemIcon>
              <Memory />
            </ListItemIcon>
            <ListItemText primary="Nodes" />
          </ListItem>
          <Divider />
        </Fragment>
      ) : null}
      {user && (
        <Fragment>
          <ListItem
            activeClassName={classes.activeListItem}
            component={NavLink}
            to="/profile"
            button
          >
            <ListItemIcon>
              <Person />
            </ListItemIcon>
            <ListItemText primary="Profile" />
          </ListItem>
          <Divider />
        </Fragment>
      )}
    </Drawer>
  );
};

AppDrawer.propTypes = {
  classes: PropTypes.shape({
    drawerPaperDefault: PropTypes.string.isRequired,
    drawerPaperClose: PropTypes.string.isRequired,
    drawerToolbar: PropTypes.string.isRequired,
    activeListItem: PropTypes.string.isRequired
  }).isRequired,
  open: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  user: PropTypes.shape({
    email: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired,
    role: PropTypes.string.isRequired,
    azureId: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired
  })
};

AppDrawer.defaultProps = {
  user: null
};

export default withStyles(styles)(AppDrawer);
