import React, { Fragment } from 'react';
import { hot } from 'react-hot-loader';
import { CssBaseline, MuiThemeProvider } from '@material-ui/core';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';
import { AppLayout, CenteredLayout, DummyMessage } from '../../components';
import store from '../../store';
import theme from '../../theme';

const fakePage = text => () => (
  <CenteredLayout>
    <DummyMessage message={text} />
  </CenteredLayout>
);

const App = () => (
  <Fragment>
    <CssBaseline />
    <Provider store={store}>
      <MuiThemeProvider theme={theme}>
        <BrowserRouter>
          <AppLayout>
            <Switch>
              <Route path="/dashboard" component={fakePage('Dashboard')} />
              <Route
                path="/charging_stations"
                component={fakePage('Charging stations')}
              />
              <Route path="/cars" component={fakePage('Cars')} />
              <Redirect to="/dashboard" />
            </Switch>
          </AppLayout>
        </BrowserRouter>
      </MuiThemeProvider>
    </Provider>
  </Fragment>
);

export default hot(module)(App);
