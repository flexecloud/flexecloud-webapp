const permissions = {
  fdm_3d_printer: 'FDM 3D printer',
  sla_3d_printer: 'SLA 3D printer',
  dlp_3d_printer: 'DLP 3D printer',
  laser_cutter: 'LASER cutter',
  cnc_milling_machine: 'CNC milling machine',
  cnc_router: 'CNC router',
  cnc_lathe: 'CNC lathe'
};

export const getPermissions = () => Object.keys(permissions);

export const getPermissionName = permission =>
  permissions[permission] || 'Unknown unicorn powers';
