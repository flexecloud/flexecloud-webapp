import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {
  AppBar,
  Button,
  IconButton,
  Toolbar,
  Typography,
  withStyles
} from '@material-ui/core';
import { Menu } from '@material-ui/icons';
import { Link } from 'react-router-dom';
import styles from './styles';

const AppMenu = ({ classes, open, toggle, user, onLogout }) => {
  const appBarClasses = classnames(classes.appBarDefault, {
    [classes.appBarShifted]: open
  });
  const menuButtonClasses = classnames(classes.buttonMenuDefault, {
    [classes.buttonMenuHidden]: open
  });
  return (
    <AppBar position="absolute" className={appBarClasses}>
      <Toolbar>
        <IconButton
          className={menuButtonClasses}
          color="inherit"
          aria-label="Toggle drawer"
          onClick={toggle}
        >
          <Menu />
        </IconButton>
        <Typography className={classes.typography} variant="h6" color="inherit">
          flexecloud
        </Typography>
        {user && (
          <Button
            component={Link}
            className={classes.buttonName}
            to="/profile"
            color="inherit"
            aria-label="Open profile page"
          >
            {user.firstName}
          </Button>
        )}
        {user ? (
          <Button
            className={classes.buttonAuth}
            onClick={onLogout}
            variant="contained"
          >
            Log out
          </Button>
        ) : (
          <Button
            className={classes.buttonAuth}
            component={Link}
            to="/login"
            variant="contained"
          >
            Log in
          </Button>
        )}
      </Toolbar>
    </AppBar>
  );
};

AppMenu.propTypes = {
  classes: PropTypes.shape({
    appBarDefault: PropTypes.string.isRequired,
    appBarShifted: PropTypes.string.isRequired,
    buttonMenuDefault: PropTypes.string.isRequired,
    buttonMenuHidden: PropTypes.string.isRequired,
    buttonAuth: PropTypes.string.isRequired,
    buttonName: PropTypes.string.isRequired,
    typography: PropTypes.string.isRequired
  }).isRequired,
  open: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  onLogout: PropTypes.func.isRequired,
  user: PropTypes.shape({
    email: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired,
    role: PropTypes.string.isRequired,
    azureId: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired
  })
};

AppMenu.defaultProps = {
  user: null
};

export default withStyles(styles)(AppMenu);
