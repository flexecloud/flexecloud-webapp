import { USER_READ_OWN, REQUEST, USER_READ_MANY } from '../../constants';
import { ENDPOINT_USER_ME, ENDPOINT_USER_USERS } from '../../endpoints';

export const readMe = () => ({
  type: USER_READ_OWN + REQUEST,
  payload: {
    request: {
      url: ENDPOINT_USER_ME()
    }
  }
});

export const readUsers = (sortBy = 'display_name') => (dispatch, getState) => {
  const { pageSize, nextPage } = getState().user;
  const offset = pageSize * nextPage;
  dispatch({
    type: USER_READ_MANY + REQUEST,
    payload: {
      request: {
        url: ENDPOINT_USER_USERS({
          limit: pageSize,
          sortBy,
          offset
        })
      }
    }
  });
};
