const tag = process.env.TAG;
const pollInterval = process.env.POLL_INTERVAL;

export const TAG = tag;
export const POLL_INTERVAL = parseInt(pollInterval, 10);

export const REQUEST = '_REQUEST';
export const SUCCESS = '_SUCCESS';
export const FAIL = '_FAIL';

export const AUTH_LOGIN = 'AUTH_LOGIN';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';

export const ISSUE_CREATE_ONE = 'ISSUE_CREATE_ONE';
export const ISSUE_UPDATE_FIELD = 'ISSUE_UPDATE_FIELD';
export const ISSUE_VALIDATE_FIELD = 'ISSUE_VALIDATE_FIELD';
export const ISSUE_CLEAR = 'ISSUE_CLEAR';

export const USER_READ_OWN = 'USER_READ_OWN';
export const USER_READ_MANY = 'USER_READ_MANY';
