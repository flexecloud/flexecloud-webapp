import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { CircularProgress, Grid, withStyles } from '@material-ui/core';
import InfiniteScroll from 'react-infinite-scroller';
import classnames from 'classnames';
import { FixedLayout } from '../../components';
import { readUsers } from '../../services/user/actions';
import { byProperty, objectToArray } from '../../util';
import { POLL_INTERVAL } from '../../constants';
import User from './components/User';
import styles from './styles';

class Users extends Component {
  constructor() {
    super();
    this.state = {
      expanded: ''
    };
    this.timer = null;
    this.onChange = this.onChange.bind(this);
    this.onLoadMore = this.onLoadMore.bind(this);
  }

  componentDidMount() {
    const { dispatchReadUsers, userIds } = this.props;
    if (!userIds.length) {
      dispatchReadUsers('display_name');
    }
    if (!this.timer) {
      this.timer = setInterval(() => {
        dispatchReadUsers('display_name');
      }, POLL_INTERVAL);
    }
  }

  componentWillUnmount() {
    clearInterval(this.timer);
    this.timer = null;
  }

  onChange(panel) {
    return () => {
      this.setState(state => ({
        expanded: state.expanded === panel ? '' : panel
      }));
    };
  }

  onLoadMore() {
    const { dispatchReadUsers } = this.props;
    dispatchReadUsers('display_name');
  }

  render() {
    const { userIds, loading, hasNextPage, classes } = this.props;
    const { expanded } = this.state;
    return (
      <FixedLayout>
        {userIds.length ? (
          <InfiniteScroll
            loadMore={this.onLoadMore}
            hasMore={!loading && hasNextPage}
            useWindow={false}
          >
            {userIds.map(userId => (
              <User
                key={userId}
                id={userId}
                expanded={expanded === userId}
                onClick={this.onChange}
              />
            ))}
          </InfiniteScroll>
        ) : null}
        {loading && (
          <Grid
            className={classnames({ [classes.loader]: userIds.length > 0 })}
            spacing={16}
            justify="center"
            container
          >
            <Grid item>
              <CircularProgress color="secondary" />
            </Grid>
          </Grid>
        )}
      </FixedLayout>
    );
  }
}

Users.propTypes = {
  userIds: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  loading: PropTypes.bool.isRequired,
  hasNextPage: PropTypes.bool.isRequired,
  dispatchReadUsers: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    loader: PropTypes.string.isRequired
  }).isRequired
};

const mapStateToProps = ({ user }) => ({
  hasNextPage: user.hasNextPage,
  loading: user.loading,
  userIds: objectToArray(user.users)
    .sort(byProperty('displayName'))
    .map(u => u.id)
});

const mapDispatchToProps = dispatch => ({
  dispatchReadUsers: sortBy => dispatch(readUsers(sortBy))
});

export default withStyles(styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Users)
);
