import defaultState from './state';
import { AUTH_LOGIN, AUTH_LOGOUT } from '../../constants';

const auth = (state = defaultState, action = {}) => {
  const { type } = action;
  switch (type) {
    case AUTH_LOGIN: {
      return Object.assign({}, state, {
        token: action.token
      });
    }
    case AUTH_LOGOUT: {
      return Object.assign({}, state, {
        token: '',
        logoutFromAzureAd: true
      });
    }
    default: {
      return state;
    }
  }
};

export default auth;
