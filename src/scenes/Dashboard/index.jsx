import React from 'react';
import {
  Card,
  CardContent,
  Button,
  CardHeader,
  Grid,
  Typography
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import { FixedLayout } from '../../components';

const Dashboard = () => (
  <FixedLayout>
    <Grid spacing={0} container>
      <Grid xs={12} sm={12} md={6} lg={6} xl={6} item>
        <Card>
          <CardHeader title="About this page" />
          <CardContent>
            <Grid spacing={16} container>
              <Grid xs={12} item>
                <Typography align="justify" gutterBottom>
                  This is the official webpage of the student workshop. It
                  offers a booking system for all manufacturing devices
                  available. To be able to use this system, you need to log in
                  with your SDU user account.
                </Typography>
              </Grid>
              <Grid item>
                <Button
                  to="/login"
                  variant="contained"
                  color="secondary"
                  component={Link}
                >
                  Log in
                </Button>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  </FixedLayout>
);

export default Dashboard;
