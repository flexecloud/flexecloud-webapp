# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.16.0] - 2018-11-25

### Changed

- Removed unnecessary content and added page mocks

## [0.15.2] - 2018-11-24

### Changed

- Removed debug statement for `${DOCKER_MACHINE_PRIVATE_KEY}`

## [0.15.1] - 2018-11-24

### Changed

- Added debug statement for `${DOCKER_MACHINE_PRIVATE_KEY}`

## [0.15.0] - 2018-11-24

### Changed

- Adjust page title to `flexecloud`

## [0.14.1] - 2018-11-24

### Changed

- Student card number only shows last digits on user profile and user detail overview

## [0.14.0] - 2018-11-22

### Added

- Update user profile in background (#14)

## [0.13.0] - 2018-11-20

### Added

- User overview page for admins (#10)

## [0.12.0] - 2018-11-19

### Added

- Informational box on dashboard to inform users about the purpose of the page (#7)

## [0.11.2] - 2018-11-09

### Fixed

- Approvals page is only visible for logged in users in sidebar menu
- Nodes page is only shown for users with role `staff`, `supervisor` or `admin` in sidebar menu

## [0.11.1] - 2018-11-09

### Fixed

- Removed dead code from previous Azure logout implementation
- Update dependencies

## [0.11.0] - 2018-11-06

### Added

- Machines page for future overview of physical machines

### Fixed

- Fixed layout breakpoints integrate smoothly with overall layout and do not jump any more

### Changed

- Use fixed layout on profile page to align overall UI feeling
- Renamed devices page to node page for future IoT node discovery and authorization

## [0.10.3] - 2018-11-05

### Fixed

- Post-deployment test delay has been increased to `20s` to ensure a stable reverse proxy setup before the test

## [0.10.2] - 2018-11-05

### Fixed

- Broken [CI configuration](./.gitlab-ci.yml) by missed out quotation mark (`"`)

## [0.10.1] - 2018-11-05

### Fixed

- Broken link to documentation by adding environment variable `DOCS_URL` in `.gitlab-ci.yml` (#5)

## [0.10.0] - 2018-11-05

### Added

- User profile shows card number

## [0.9.0] - 2018-11-05

### Changed

- Improve browser compatibility via `.browserslistrc`

### Added

- Feedback form on help page to submit issues to GitLab with simple issue preview
- Link to documentation on help page

## [0.8.0] - 2018-10-28

### Added

- Login page with login button
- Background logout via hidden `<iframe />`
- Profile view with basic user information
- Persistent login via usage of cookies
- HOC (higher order component) for protected views
- Automatic logout in case of unauthenticated requests

### Removed

- Unnecessary installation of `npm` CI to reduce deployment time

## [0.7.6] - 2018-10-14

### Fixed

- Removed scrollbar when layout should display content centered

## [0.7.5] - 2018-10-14

### Fixed

- Overflowing content in application layout displays proper scrollbar

## [0.7.4] - 2018-10-12

### Changed

- Silence `curl` output in CI pipeline

## [0.7.3] - 2018-10-12

### Fixed

- Typo in `CHANGELOG.md`

## [0.7.2] - 2018-10-12

### Changed

- Added delay before post-deployment test to allow deployment to settle

## [0.7.1] - 2018-10-12

### Fixed

- CI stage `deploy` requires `curl`, which was not previously installed

## [0.7.0] - 2018-10-12

### Changed

- Adjust page title to `SDU - The Core`
- Use different icon for devices

### Added

- Connect deployment to environment in GitLab

## [0.6.0] - 2018-10-12

### Added

- Set up `react-router-dom`
- Cross-linking of different views

## [0.5.1] - 2018-10-11

### Added

- Install dependencies via `npm ci` to enforce aligned `package-lock.json`

## [0.5.0] - 2018-10-11

### Added

- Deployment to Docker host via [.gitlab-ci.yml](./.gitlab-ci.yml)

### Changed

- Adjusted CI variable `PROD_API_URL` to be `API_URL` now

## [0.4.0] - 2018-10-08

### Added

- Expandable menu drawer
- Basic page layout

## [0.3.1] - 2018-10-08

### Fixed

- Removed warning about unsupported hot reloading from developer console

## [0.3.0] - 2018-10-08

### Added

- AppBar on front page
- `.env` file requires values `TAG` and `API_URL`

### Fixed

- `main` entry in `package.json` reflects entry for React application

### Changed

- Dependency upgrade check uses `david` instead of `npm-check-updates`
- Automatic tagging will check `version` in `package.json` and `package-lock.json`

## [0.2.0] - 2018-09-16

### Added

- Documentation for usage and tooling
- Cache for npm dependencies in CI
- Docker image build
- Automatic tagging in CI

## [0.1.0] - 2018-09-14

### Added

- [Webpack](https://webpack.js.org/) setup for common [React](https://reactjs.org/) app
- Setup of [Material UI](https://material-ui.com/)
- Linting with [ESLint](https://eslint.org/)
- Code formatting with [Prettier](https://prettier.io/)
- [MIT License](https://mit-license.org)
- CI with `.gitlab-ci.yml`
- Pre-commit hooks for formatting, linting and testing
- Hot module replacement via [react-hot-loader](https://github.com/gaearon/react-hot-loader)
