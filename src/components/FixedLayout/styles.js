const styles = theme => ({
  root: {
    margin: 0,
    [theme.breakpoints.up(
      theme.breakpoints.values.lg + theme.drawer.width + 4 * theme.spacing.unit
    )]: {
      width: theme.breakpoints.values.lg,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  }
});

export default styles;
