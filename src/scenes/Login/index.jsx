import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Grid,
  LinearProgress
} from '@material-ui/core';
import queryString from 'query-string';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { CenteredLayout } from '../../components';
import { ENDPOINT_OAUTH2_AZURE_AD_LOGIN } from '../../endpoints';
import { login, logout } from '../../services/auth/actions';

class Login extends Component {
  constructor() {
    super();
    this.state = {
      loading: false
    };
    this.onClick = this.onClick.bind(this);
  }

  componentDidMount() {
    this.authorize();
  }

  componentDidUpdate() {
    this.authorize();
  }

  onClick() {
    const { dispatchLogout, auth } = this.props;
    if (auth.token) {
      dispatchLogout();
    } else {
      this.setState({
        loading: true
      });
    }
  }

  authorize() {
    const { dispatchLogin, history } = this.props;
    const { location, replace } = history;
    if (location.hash.length > 1) {
      const { token } = queryString.parse(location.hash);
      dispatchLogin(token);
      replace(location.pathname);
    }
  }

  render() {
    const { loading } = this.state;
    const { auth } = this.props;
    const { token } = auth;
    const href = !token ? ENDPOINT_OAUTH2_AZURE_AD_LOGIN() : undefined;
    const component = !token ? 'a' : undefined;
    const title = token ? 'Log out' : 'Log in';
    const subheader = token
      ? 'Remove access to your identity'
      : 'Authorize access to your identity';
    return (
      <CenteredLayout>
        <Grid xs={12} sm={10} md={8} lg={6} xl={4} item>
          <Card>
            {loading && <LinearProgress color="secondary" />}
            <CardHeader title={title} subheader={subheader} />
            <CardContent>
              <Button
                onClick={this.onClick}
                disabled={loading}
                href={href}
                component={component}
                variant="contained"
                color="primary"
                fullWidth
              >
                {token ? 'Log out from SDU' : 'Log in with SDU'}
              </Button>
            </CardContent>
          </Card>
        </Grid>
      </CenteredLayout>
    );
  }
}

Login.propTypes = {
  dispatchLogin: PropTypes.func.isRequired,
  dispatchLogout: PropTypes.func.isRequired,
  auth: PropTypes.shape({
    token: PropTypes.string.isRequired
  }).isRequired,
  history: PropTypes.shape({
    location: PropTypes.shape({
      hash: PropTypes.string.isRequired,
      pathname: PropTypes.string.isRequired
    }).isRequired,
    replace: PropTypes.func.isRequired
  }).isRequired
};

const mapStateToProps = ({ auth }) => ({
  auth
});

const mapDispatchToProps = dispatch => ({
  dispatchLogin: token => dispatch(login(token)),
  dispatchLogout: () => dispatch(logout())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Login));
