module.exports = {
  setupTestFrameworkScriptFile: require.resolve('./unit.jest.setup.js'),
  rootDir: '../',
  transform: {
    '^.+\\.jsx?$': 'babel-jest'
  },
  collectCoverage: true,
  collectCoverageFrom: ['src/**.{js,jsx}', '!src/index.jsx'],
  coverageReporters: ['text']
};
