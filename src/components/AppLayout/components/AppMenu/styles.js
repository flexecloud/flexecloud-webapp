const styles = theme => ({
  appBarDefault: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  appBarShifted: {
    marginLeft: theme.drawer.width,
    width: `calc(100% - ${theme.drawer.width}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  buttonMenuDefault: {
    marginLeft: -theme.spacing.unit * 1.5,
    marginRight: theme.spacing.unit * 4
  },
  buttonMenuHidden: {
    display: 'none'
  },
  buttonAuth: {
    marginRight: theme.spacing.unit * 2,
    backgroundColor: theme.palette.primary.contrastText,
    color: theme.palette.primary.main
  },
  buttonName: {
    marginRight: theme.spacing.unit * 2
  },
  typography: {
    flexGrow: 1
  }
});

export default styles;
