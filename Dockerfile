### build process ###
# base image
FROM node:8-alpine as build

# working directory
WORKDIR /usr/src/app

# copy dependency lists
COPY package.json package-lock.json ./

# install dependencies
RUN npm i

# copy source files
COPY . ./

# build time variables
ARG API_URL
ARG TAG
ARG POLL_INTERVAL

# build application
RUN npm run build

### production environment ###
# base image
FROM nginx:mainline-alpine

# remove default virtual host configuration
RUN mv /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.bak

# copy optimized virtual host configuration
COPY config/nginx.conf /etc/nginx/conf.d/default.conf

# copy files from previous stage
COPY --from=build /usr/src/app/dist /usr/share/nginx/html

# expose default nginx port
EXPOSE 80

# start nginx server
CMD ["nginx", "-g", "daemon off;"]
