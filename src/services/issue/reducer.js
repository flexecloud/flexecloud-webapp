import { get } from 'lodash';
import defaultState from './state';
import {
  ISSUE_UPDATE_FIELD,
  ISSUE_VALIDATE_FIELD,
  ISSUE_CREATE_ONE,
  ISSUE_CLEAR,
  REQUEST,
  SUCCESS,
  FAIL
} from '../../constants';

const errors = (state = defaultState, action = {}) => {
  const { type } = action;
  switch (type) {
    case ISSUE_UPDATE_FIELD: {
      const newErrors = [];
      const validTypes = ['kind/feature', 'kind/bug'];
      if (action.field === 'type') {
        if (!state.form.errors.find(err => err.field === 'type')) {
          if (!validTypes.includes(action.value)) {
            newErrors.push({
              field: 'type',
              error: 'A valid type has to be selected.'
            });
            return [...state.form.errors, ...newErrors];
          }
        }
      }
      return state.form.errors.filter(err => err.field !== action.field);
    }
    case ISSUE_VALIDATE_FIELD: {
      const newErrors = [...state.form.errors];
      if (action.field === 'title') {
        if (state.form.fields.title.length < 10) {
          if (!state.form.errors.find(err => err.field === action.field)) {
            newErrors.push({
              field: 'title',
              error: 'The title has to be at least 10 characters long.'
            });
          }
        } else {
          newErrors.forEach((err, index) => {
            if (err.field === action.field) {
              newErrors.splice(index, 1);
            }
          });
        }
      }
      if (action.field === 'description') {
        if (state.form.fields.description.length < 20) {
          if (!state.form.errors.find(err => err.field === action.field)) {
            newErrors.push({
              field: 'description',
              error: 'The description has to be at least 20 characters long.'
            });
          }
        } else {
          newErrors.forEach((err, index) => {
            if (err.field === action.field) {
              newErrors.splice(index, 1);
            }
          });
        }
      }
      return [...newErrors];
    }
    default: {
      return state.form.errors;
    }
  }
};

const fields = (state = defaultState, action = {}) => {
  const { type } = action;
  switch (type) {
    case ISSUE_UPDATE_FIELD: {
      return Object.assign({}, state.form.fields, {
        [action.field]: action.value
      });
    }
    case ISSUE_CREATE_ONE + REQUEST + SUCCESS: {
      return Object.assign({}, state.form.fields, {
        type: '',
        title: '',
        description: ''
      });
    }
    default: {
      return state.form.fields;
    }
  }
};

const form = (state = defaultState, action = {}) => {
  const { type } = action;
  switch (type) {
    case ISSUE_UPDATE_FIELD:
    case ISSUE_VALIDATE_FIELD: {
      return Object.assign({}, state.form, {
        fields: fields(state, action),
        errors: errors(state, action)
      });
    }
    case ISSUE_CREATE_ONE + REQUEST: {
      return Object.assign({}, state.form, {
        loading: true
      });
    }
    case ISSUE_CREATE_ONE + REQUEST + FAIL: {
      return Object.assign({}, state.form, {
        loading: false
      });
    }
    case ISSUE_CREATE_ONE + REQUEST + SUCCESS: {
      return Object.assign({}, state.form, {
        fields: fields(state, action),
        loading: false
      });
    }
    default: {
      return state.form;
    }
  }
};

const issue = (state = defaultState, action = {}) => {
  const { type } = action;
  switch (type) {
    case ISSUE_UPDATE_FIELD:
    case ISSUE_VALIDATE_FIELD:
    case ISSUE_CREATE_ONE + REQUEST:
    case ISSUE_CREATE_ONE + REQUEST + FAIL: {
      return Object.assign({}, state, {
        form: form(state, action)
      });
    }
    case ISSUE_CREATE_ONE + REQUEST + SUCCESS: {
      return Object.assign({}, state, {
        form: form(state, action),
        issue: get(action, 'payload.data.data') || null
      });
    }
    case ISSUE_CLEAR: {
      return Object.assign({}, state, {
        issue: null
      });
    }
    default: {
      return state;
    }
  }
};

export default issue;
