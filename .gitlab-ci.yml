stages:
  - test
  - tag
  - build
  - deploy
cache:
  key: ${CI_PROJECT_PATH_SLUG}-npm
  paths:
    - node_modules/
test:
  stage: test
  image: node:10-alpine
  except:
    - tags
  script:
    # install david
    - npm i -g david
    # check if dependencies can be updated
    - david
    # install dependencies
    - npm ci
    # lint code
    - npm run lint:check
    # execute unit test suite
    - npm run test:unit
    # build static assets
    - npm run build
tag:
  stage: tag
  dependencies:
    - test
  only:
    refs:
      - master
  image: bash
  script:
    # install git
    - apk add --no-cache git git-lfs jq
    # get latest changelog version
    - VERSION_CHANGELOG=$(grep -m 1 '## \[' CHANGELOG.md | awk '{ print $2; }' | tr -d '[]')
    # get latest git tag
    - VERSION_GIT=$(git describe --abbrev=0 --tags)
    # get version from package.json
    - VERSION_PACKAGE=$(cat package.json | jq -r ".version")
    # get version from packge-lock.json
    - VERSION_PACKAGE_LOCK=$(cat package-lock.json | jq -r ".version")
    # tagging logic
    - |
      # tag if changelog version does not exist yet and is not 'Unreleased'
      if [ "${VERSION_CHANGELOG}" != "${VERSION_GIT}" ]; then
        # check if version in CHANGELOG.md is 'Unreleased'
        if [ "${VERSION_CHANGELOG}" != "Unreleased" ]; then
          # tag if version is not 'Unreleased'
          if [ "${VERSION_CHANGELOG}" != "${VERSION_PACKAGE}" ]; then
            echo "Version in package.json must match release version."
            echo "Aborting creation of tag."
            exit 1
          fi
          if [ "${VERSION_CHANGELOG}" != "${VERSION_PACKAGE_LOCK}" ]; then
            echo "Version in package-lock.json must match release version."
            echo "Aborting creation of tag."
            exit 1
          fi
          echo Creating new tag.
          git config user.name "${CI_BOT_GIT_NAME}"
          git config user.email "${CI_BOT_GIT_EMAIL}"
          git remote set-url origin https://"${CI_BOT_AUTH_USERNAME}:${CI_BOT_AUTH_PASSWORD}"@gitlab.com/${CI_PROJECT_PATH}.git
          git tag -a ${VERSION_CHANGELOG} -m "For changes, see the CHANGELOG.md."
          git push --tags
        else
          # do not tag if version is 'Unreleased'
          echo Version in CHANGELOG.md is 'Unreleased'.
          echo This commit will not trigger a release.
        fi
      else
        # do not tag if version already exists
        echo Version in CHANGELOG.md already exists.
        echo This commit will not trigger a release.
      fi
build:
  stage: build
  only:
    - tags
  image: docker:git
  services:
    - docker:dind
  script:
    # get latest tag
    - VERSION_GIT=$(git describe --abbrev=0 --tags)
    # login to docker gitlab docker registry
    - docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
    # build and tag image
    - |
      docker build \
      --build-arg TAG=${VERSION_GIT} \
      --build-arg API_URL=${API_URL} \
      --build-arg POLL_INTERVAL=${POLL_INTERVAL} \
      -t ${CI_REGISTRY}/${CI_PROJECT_PATH}:${VERSION_GIT} \
      -t ${CI_REGISTRY}/${CI_PROJECT_PATH}:latest .
    # push tags to gitlab docker registry
    - docker push ${CI_REGISTRY}/${CI_PROJECT_PATH}:${VERSION_GIT}
    - docker push ${CI_REGISTRY}/${CI_PROJECT_PATH}:latest
    # login from docker gitlab docker registry
    - docker logout ${CI_REGISTRY}
deploy:
  stage: deploy
  only:
    - tags
  environment:
    name: prod
  image: bash
  script:
    # install git
    - apk add --no-cache git git-lfs curl
    # get latest tag
    - VERSION_GIT=$(git describe --abbrev=0 --tags)
    # create .ssh directory
    - mkdir -p ~/.ssh
    # adjust permissions of .ssh directory
    - chmod 700 ~/.ssh
    # set up known_hosts to prevent man-in-the-middle attacks
    - echo "${DOCKER_MACHINE_KNOWN_HOSTS}" > ~/.ssh/known_hosts
    # adjust permissions of known_hosts
    - chmod 644 ~/.ssh/known_hosts
    # check if SSH client is installed
    - which ssh-agent || (apk add --no-cache openssh-client)
    # verify successful installation of openssh-client
    - eval $(ssh-agent -s)
    # configure ssh keys for deployment
    - echo "${DOCKER_MACHINE_PRIVATE_KEY}" | tr -d '\r' | ssh-add - > /dev/null
    # stop existing container
    - ssh ${DOCKER_MACHINE_USER}@${DOCKER_MACHINE_HOST} docker stop app-${CI_PROJECT_NAME} || true
    # remove existing container
    - ssh ${DOCKER_MACHINE_USER}@${DOCKER_MACHINE_HOST} docker rm app-${CI_PROJECT_NAME} || true
    # pull new container
    - ssh ${DOCKER_MACHINE_USER}@${DOCKER_MACHINE_HOST} docker pull ${CI_REGISTRY}/${CI_PROJECT_PATH}:${VERSION_GIT}
    # start new container
    - |
      ssh ${DOCKER_MACHINE_USER}@${DOCKER_MACHINE_HOST} \
      docker run -d \
      --restart always \
      -e VIRTUAL_HOST=${APP_URL} \
      -e LETSENCRYPT_HOST=${APP_URL} \
      -e LETSENCRYPT_EMAIL=${LETSENCRYPT_EMAIL} \
      --name app-${CI_PROJECT_NAME} ${CI_REGISTRY}/${CI_PROJECT_PATH}:${VERSION_GIT}
    # wait for deployment to settle
    - sleep 20
    # verify successful deployment
    - curl -sSL ${CI_ENVIRONMENT_URL} | grep "flexecloud" > /dev/null
