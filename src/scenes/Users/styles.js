const styles = theme => ({
  loader: {
    marginTop: theme.spacing.unit * 2
  }
});

export default styles;
