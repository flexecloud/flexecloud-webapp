import { snakeCase } from 'lodash';

// use separate variable
// linter prefers destructuring
// destructuring does not work with the webpack environment plugin
const apiUrl = process.env.API_URL;
const docsUrl = process.env.DOCS_URL;

export const API_URL = apiUrl;
export const DOCS_URL = docsUrl;

// azure oauth2 authentification endpoints
export const ENDPOINT_OAUTH2_AZURE_AD_LOGIN = () =>
  `${apiUrl}/v1/auth/azure_ad`;
export const ENDPOINT_OAUTH2_AZURE_AD_LOGOUT = redirectUri => {
  const endpoint = 'https://login.microsoftonline.com/common/oauth2/logout';
  const search = `?post_logout_redirect_uri=${redirectUri}`;
  return redirectUri ? endpoint + search : endpoint;
};

// user endpoints
export const ENDPOINT_USER_ME = () => '/me';
export const ENDPOINT_USER_USERS = pagination => {
  if (!pagination) return '/users';
  const query = Object.keys(pagination)
    .map(param => `${snakeCase(param)}=${pagination[param]}`)
    .join('&');
  if (query) return `/users?${query}`;
  return '/users';
};

// issue endpoints
export const ENDPOINT_ISSUE = id => (id ? `/issues/${id}` : '/issues');
