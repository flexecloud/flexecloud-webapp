import React from 'react';
import PropTypes from 'prop-types';
import { Typography, Grid } from '@material-ui/core';

const DummyMessage = ({ message }) => (
  <Grid item>
    <Typography variant="h1">{message}</Typography>
  </Grid>
);

DummyMessage.propTypes = {
  message: PropTypes.string.isRequired
};

export default DummyMessage;
