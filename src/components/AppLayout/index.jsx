import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { AppMenu, AppDrawer } from './components';
import styles from './styles';
import { readMe } from '../../services/user/actions';
import { logout } from '../../services/auth/actions';
import { ENDPOINT_OAUTH2_AZURE_AD_LOGOUT } from '../../endpoints';

class AppLayout extends Component {
  constructor() {
    super();
    this.state = {
      open: true
    };
    this.toggle = this.toggle.bind(this);
    this.logout = this.logout.bind(this);
  }

  componentDidMount() {
    this.getMeIfAuthorized();
  }

  componentDidUpdate() {
    this.getMeIfAuthorized();
  }

  getMeIfAuthorized() {
    const { auth, user, dispatchReadMe } = this.props;
    if (auth.token && !user.user) {
      dispatchReadMe();
    }
  }

  logout() {
    const { dispatchLogout } = this.props;
    dispatchLogout();
    this.setState(state =>
      Object.assign({}, state, {
        logoutFromAzure: true
      })
    );
  }

  toggle() {
    this.setState(state => ({
      open: !state.open
    }));
  }

  render() {
    const { open } = this.state;
    const { children, classes, user, auth } = this.props;
    return (
      <div className={classes.layout}>
        <AppMenu
          user={user.user}
          open={open}
          onLogout={this.logout}
          toggle={this.toggle}
        />
        <AppDrawer user={user.user} open={open} toggle={this.toggle} />
        <main className={classes.main}>{children}</main>
        {auth.logoutFromAzureAd && (
          <iframe
            title="azureAdBackgroundLogout"
            className={classes.iframe}
            src={ENDPOINT_OAUTH2_AZURE_AD_LOGOUT()}
          />
        )}
      </div>
    );
  }
}

AppLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired,
  classes: PropTypes.shape({
    layout: PropTypes.string.isRequired,
    main: PropTypes.string.isRequired,
    iframe: PropTypes.string.isRequired
  }).isRequired,
  dispatchReadMe: PropTypes.func.isRequired,
  dispatchLogout: PropTypes.func.isRequired,
  auth: PropTypes.shape({
    token: PropTypes.string.isRequired,
    logoutFromAzureAd: PropTypes.bool.isRequired
  }).isRequired,
  user: PropTypes.shape({
    user: PropTypes.shape({
      email: PropTypes.string.isRequired,
      firstName: PropTypes.string.isRequired,
      lastName: PropTypes.string.isRequired,
      displayName: PropTypes.string.isRequired,
      role: PropTypes.string.isRequired,
      azureId: PropTypes.string.isRequired,
      id: PropTypes.string.isRequired
    })
  }).isRequired
};

const mapStateToProps = ({ auth, user }) => ({ auth, user });
const mapDispatchToProps = dispatch => ({
  dispatchReadMe: () => dispatch(readMe()),
  dispatchLogout: () => dispatch(logout())
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withStyles(styles)(AppLayout))
);
