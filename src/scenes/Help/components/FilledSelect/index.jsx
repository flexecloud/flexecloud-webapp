import React from 'react';
import PropTypes from 'prop-types';
import {
  FilledInput,
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select,
  withStyles
} from '@material-ui/core';
import uuidv4 from 'uuid/v4';
import styles from './styles';

const FilledSelect = ({
  label,
  options,
  value,
  onChange,
  name,
  id,
  fullWidth,
  disabled,
  classes,
  error,
  helperText,
  required
}) => (
  <FormControl
    className={classes.formControl}
    disabled={!options.length || disabled}
    fullWidth={fullWidth}
    error={error}
    variant="filled"
  >
    {(label || !options.length) && (
      <InputLabel htmlFor={id} error={error} required={required}>
        {options.length ? label : 'No options available'}
      </InputLabel>
    )}
    <Select
      value={value}
      onChange={onChange}
      input={<FilledInput name={name} id={id} />}
    >
      <MenuItem value="none">
        <em>None</em>
      </MenuItem>
      {options.length &&
        options.map(option => (
          <MenuItem key={option.label} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
    </Select>
    {helperText && <FormHelperText>{helperText}</FormHelperText>}
  </FormControl>
);

FilledSelect.propTypes = {
  value: PropTypes.string,
  label: PropTypes.string,
  id: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func,
  fullWidth: PropTypes.bool,
  disabled: PropTypes.bool,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired
    }).isRequired
  ),
  classes: PropTypes.shape({
    formControl: PropTypes.string.isRequired
  }).isRequired,
  error: PropTypes.bool,
  helperText: PropTypes.string,
  required: PropTypes.bool
};

FilledSelect.defaultProps = {
  id: uuidv4(),
  name: '',
  disabled: false,
  fullWidth: false,
  value: 'none',
  label: '',
  onChange: () => null,
  options: [],
  error: false,
  required: false,
  helperText: ''
};

export default withStyles(styles)(FilledSelect);
