import React from 'react';
import { render } from 'react-dom';
import App from './scenes/App';

window.TAG = process.env.TAG;
window.API_URL = process.env.API_URL;

render(<App />, document.getElementById('app'));
