import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

function AuthN(InsecureComponent) {
  class SecureComponent extends React.Component {
    componentDidMount() {
      this.verifyAuthorization();
    }

    componentDidUpdate() {
      this.verifyAuthorization();
    }

    verifyAuthorization() {
      const { auth, history } = this.props;
      if (!auth.token) {
        history.push('/login');
      }
    }

    render() {
      const { auth, ...rest } = this.props;
      if (!auth.token) {
        return null;
      }
      return <InsecureComponent {...rest} />;
    }
  }

  SecureComponent.propTypes = {
    history: PropTypes.shape({
      push: PropTypes.func.isRequired
    }).isRequired,
    auth: PropTypes.shape({
      token: PropTypes.string.isRequired
    }).isRequired
  };

  const mapStateToProps = ({ auth }) => ({ auth });

  return connect(mapStateToProps)(withRouter(SecureComponent));
}

export default AuthN;
