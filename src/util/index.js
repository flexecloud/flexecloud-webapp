export { walkKeys, walkValues } from './walk';
export { getPermissionName, getPermissions } from './permissions';
export { objectToArray, arrayToObject } from './normalize';

export const byProperty = property => (a, b) => {
  if (a[property] < b[property]) return -1;
  if (a[property] > b[property]) return 1;
  return 0;
};
