import React from 'react';
import { render } from 'react-testing-library';
import DummyMessage from './index';

describe('DummyMessage', () => {
  it('shows the correct text', () => {
    // arrange
    const message = 'Hello, The Core!';

    // act
    const { getByText, container } = render(<DummyMessage message={message} />);

    // assert
    expect(getByText(message)).not.toBeNull();
    expect(container).toMatchSnapshot();
  });
});
