import { createMuiTheme } from '@material-ui/core';

export default createMuiTheme({
  palette: {
    primary: {
      main: '#3f51b5'
    },
    secondary: {
      main: '#ef6c00'
    },
    text: {
      disabled: 'rgba(0, 0, 0, 0.54)'
    }
  },
  typography: {
    useNextVariants: true
  },
  drawer: {
    width: 300
  }
});
