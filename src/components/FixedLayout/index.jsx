import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { withStyles } from '@material-ui/core';
import styles from './styles';

const FixedLayout = ({ children, classes, className }) => (
  <div className={classnames(classes.root, className)}>{children}</div>
);

FixedLayout.propTypes = {
  className: PropTypes.string,
  classes: PropTypes.shape({
    root: PropTypes.string.isRequired
  }).isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

FixedLayout.defaultProps = {
  className: ''
};

export default withStyles(styles)(FixedLayout);
